package ch.teko;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Family {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String description;

    @OneToMany(mappedBy = "family", cascade = CascadeType.PERSIST)
    private List<Person> members = new ArrayList<Person>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addMember(Person member){
        members.add(member);
        member.setFamily(this);
    }

    public void removeMember(Person member){
        members.remove(member);
        member.setFamily(null);
    }

    public List<Person> getMembers() {
        return members;
    }
}
