import ch.teko.Family;
import ch.teko.Person;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;

public class TestJPA {

    private EntityManagerFactory factory;

    @Before
    public void setUp(){
        factory = Persistence.createEntityManagerFactory("people");
        EntityManager em = factory.createEntityManager();

        Family family = new Family();
        family.setDescription("Familie Baeni");

        Person person1 = new Person();
        person1.setFirstName("Benjamin");
        person1.setLastName("Baeni");
        family.addMember(person1);
        Person person2 = new Person();
        person2.setFirstName("Corinne");
        person2.setLastName("Baeni");
        family.addMember(person2);

        em.getTransaction().begin();
        em.persist(family);

        em.getTransaction().commit();
    }

    @Test
    public void assertFamily(){
        // Arrange
        EntityManager em = factory.createEntityManager();
        Query query = em.createQuery("select f from Family f");
        // Act
        List<Family> resultList = query.getResultList();
        // Assert
        Assert.assertThat(resultList, Matchers.hasSize(1));
        Assert.assertThat(resultList.get(0).getMembers(), Matchers.hasSize(2));
    }
}
